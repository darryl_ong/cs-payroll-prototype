package com.cloudsherpas.payroll.rule.engine.service;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.cloudsherpas.payroll.rule.engine.dto.EmployeePayrollDetails;

public class RulesService {
	public void runRules(EmployeePayrollDetails epd){
		// load up the knowledge base
        KnowledgeBase kbase;
		try {
			kbase = readKnowledgeBase();
			StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
	        
	        ksession.insert(epd);
	        ksession.fireAllRules();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static KnowledgeBase readKnowledgeBase() throws Exception {
		   
	      KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
	      
	      kbuilder.add(ResourceFactory.newClassPathResource("ph_rules.drl"), ResourceType.DRL);
	      
	      KnowledgeBuilderErrors errors = kbuilder.getErrors();
	      
	      if (errors.size() > 0) {
	         for (KnowledgeBuilderError error: errors) {
	            System.err.println(error);
	         }
	         throw new IllegalArgumentException("Could not parse knowledge.");
	      }
	      
	      KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
	      kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
	      
	      return kbase;
	   }
}
