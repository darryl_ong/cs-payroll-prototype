package com.cloudsherpas.payroll.rule.engine.api;

import com.cloudsherpas.payroll.rule.engine.dto.EmployeePayrollDetails;
import com.cloudsherpas.payroll.rule.engine.service.PayrollService;
import com.cloudsherpas.payroll.rule.engine.service.RulesService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

@Api(
        name = "payrollRuleEngine",
        version = "v1",
        description = "Sample API for Payroll Rule Engine"
)
public class PayrollRuleEngineApi {
	@ApiMethod(
            name = "getSalary",
            path = "salary",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public EmployeePayrollDetails getSalary(EmployeePayrollDetails epd){
		return new PayrollService().processPayroll(epd);
	}
}
