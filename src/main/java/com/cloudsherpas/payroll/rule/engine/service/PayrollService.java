package com.cloudsherpas.payroll.rule.engine.service;

import com.cloudsherpas.payroll.rule.engine.dto.EmployeePayrollDetails;

public class PayrollService {
	public EmployeePayrollDetails processPayroll(EmployeePayrollDetails epd){
		new RulesService().runRules(epd);
		return epd;
	}
}
